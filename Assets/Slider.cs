﻿using UnityEngine;
using System.Collections;

public class Slider : MonoBehaviour {

	public float spacing = 10f;
	public GameObject[] row;

	public float dir = 1f;
	float offset;
	Vector3 v;
	// Use this for initialization
	void Start () {
		v = transform.position;
		Layout();
		Screen.showCursor = false;
	}

	void Layout(){
		offset = (row.Length - 1) * spacing * 0.5f;
		for(int i = 0; i < row.Length; i++){
			row[i].transform.parent = transform;
			row[i].transform.localPosition = new Vector3(i * spacing - offset, 0f, 0f);
		}
	}
	
	// Update is called once per frame


	void Update () {
		v.x += dir * Time.deltaTime;
		transform.position = v;
		if(transform.position.x < -offset * transform.localScale.x) dir *= -1;
		if(transform.position.x > offset * transform.localScale.x) dir *= -1;
	}
}
